<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PrayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prayers')->delete();
 
        $prayers = array(
            ['id' => 1, 'name'=>'Fajr', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name'=>'Zuhr', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'name'=>'Ashr', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'name'=>'Maghrib', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 5, 'name'=>'Isya', 'created_at' => new DateTime, 'updated_at' => new DateTime],
          );
 
        // Uncomment the below to run the seeder
        DB::table('prayers')->insert($prayers);
    }

}