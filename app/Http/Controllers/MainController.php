<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MainController extends Controller
{

   public function postRegister(Request $request){
        $user     = new User;
        $user->name = $request->input('name');
        $user->password = Hash::make($request->input('password'));
        $user->gender   = $request->input('gender');
        $user->email    = $request->input('email');
        $success= $user->save();
        if($success){
            return response()->json(['success'=>'user registered']);
        }
        else{
            return response()->json(['error'=>'registration failed']);
        } 
   }

   public function postAuthuser(Request $request){
        $username = $request->input('name');
        $password = Hash::make($request->input('password'));
        $user     = User::where('name',=,$username)
                          ->where('password',=,$password)
                          ->get();
        if($user){
            return response()->json($user);
        }
        else{
            return response()->json(['error'=>'login failed']);
        }                       
   } 
   public function postInsertscore(Request $request){
        $score              = new Score;
        $score->user_id     = $request->input('user_id');
        $score->prayer_id   = $request->input('prayer_id');
        $score->score       = $request->input('score');

        if($success){
            return response()->json(['success'=>'record inserted']);
        }
        else{
            return response()->json(['error'=>'data insertion failed']);
        } 
   }

   public function getDailyscore(Request $request){
        $user_id = $request->input('user_id');
        $score   = Score::join('prayers','scores.prayer_id','=','prayers.id')
                          ->select('prayers.name','score')
                          ->where('user_id',=,$user_id)
                          ->whereRaw('date(created_at) = curdate()')
                          ->orderBy('prayer_id')
                          ->get();

        if($score){
            return response()->json($score);
        }
        else{
            return response()->json(['error'=>'record not found']);
        } 
   }

   public function getWeeklyscore(Request $request){
        $user_id = $request->input('user_id');
        $score   = Score::join('prayers','scores.prayer_id','=','prayers.id')
                          ->select('prayers.name','score')
                          ->where('user_id',=,$user_id)
                          ->whereRaw('WEEK(created_at) = WEEK(NOW())')
                          ->orderBy('prayer_id')
                          ->get();

        if($score){
            return response()->json($score);
        }
        else{
            return response()->json(['error'=>'record not found']);
        } 
   }

   public function getMonthlyscore(Request $request){
        $user_id = $request->input('id');
        $score   = Score::join('prayers','scores.prayer_id','=','prayers.id')
                          ->select('prayers.name','score')
                          ->where('user_id',=,$user_id)
                          ->whereRaw('MONTH(created_at) = MONTH(NOW())')
                          ->orderBy('prayer_id')
                          ->get();
   
        if($score){
            return response()->json($score);
        }
        else{
            return response()->json(['error'=>'record not found']);
        } 
   }

   
}
