<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Score;
use App\Http\Requests\Scores\CreateScoreRequest;
use App\Http\Requests\Scores\UpdateScoreRequest;

class ScoresController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $scores = Score::latest()->paginate(20);

        $no = $scores->firstItem();

        return view('scores.index', compact('scores', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('scores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateScoreRequest $request)
    {
        $score = Score::create($request->all());

        return redirect()->route('scores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $score = Score::findOrFail($id);

        return view('scores.show', compact('score'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $score = Score::findOrFail($id);
    
        return view('scores.edit', compact('score'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateScoreRequest $request, $id)
    {       
        $score = Score::findOrFail($id);

        $score->update($request->all());

        return redirect()->route('scores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $score = Score::findOrFail($id);
        
        $score->delete();
    
        return redirect()->route('scores.index');
    }

}
