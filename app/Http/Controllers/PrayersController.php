<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Prayer;
use App\Http\Requests\Prayers\CreatePrayerRequest;
use App\Http\Requests\Prayers\UpdatePrayerRequest;

class PrayersController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $prayers = Prayer::latest()->paginate(20);

        $no = $prayers->firstItem();

        return view('prayers.index', compact('prayers', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('prayers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreatePrayerRequest $request)
    {
        $prayer = Prayer::create($request->all());

        return redirect()->route('prayers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $prayer = Prayer::findOrFail($id);

        return view('prayers.show', compact('prayer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $prayer = Prayer::findOrFail($id);
    
        return view('prayers.edit', compact('prayer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdatePrayerRequest $request, $id)
    {       
        $prayer = Prayer::findOrFail($id);

        $prayer->update($request->all());

        return redirect()->route('prayers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $prayer = Prayer::findOrFail($id);
        
        $prayer->delete();
    
        return redirect()->route('prayers.index');
    }

}
